package com.example.kostik.taskignitix.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.kostik.taskignitix.R;
import com.example.kostik.taskignitix.adapters.ImageAdapter;
import com.example.kostik.taskignitix.models.BitmapWrap;
import com.example.kostik.taskignitix.other.Task;
import com.example.kostik.taskignitix.views.CameraPreview;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kostik on 14.9.17.
 */

public class MainFragment extends Fragment implements Camera.PictureCallback{
    private static final String TAG = "testtest";
    private static final String INTERNAL_STORAGE_PATH = "/data/data/com.example.kostik.taskignitix/app_msa_pic/";

    private Camera camera;
    private CameraPreview cameraPreview;

    private ImageView snapshotIcon,deleteIcon,discardIcon,flashIcon;

    private ArrayList<BitmapWrap> bitmaps;
    private RecyclerView recyclerView;
    private ImageAdapter adapter;
    private BitmapWrap currentBitmapWrap;
    private int currentBitmapWrapPosition;
    private FrameLayout mainLayout;
    
    private boolean cameraShowFlag;
    private boolean flashFlag;
    private boolean cameraReadyFlag;
    private View mainView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mainView = inflater.inflate(R.layout.fragment_main, container, false);

        cameraShowFlag = true;
        flashFlag = false;
        cameraReadyFlag = true;
        currentBitmapWrap = null;
        currentBitmapWrapPosition = -1;

        //icons
        snapshotIcon = mainView.findViewById(R.id.snapshot);
        deleteIcon = mainView.findViewById(R.id.delete);
        discardIcon = mainView.findViewById(R.id.discard);
        flashIcon = mainView.findViewById(R.id.flash);

        bitmaps = new ArrayList<BitmapWrap>();

        if(Build.VERSION.SDK_INT >=  23){
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        }
        else{
            cameraInit();

            recycleViewInit();

            loadBitmapsRecycleView();

            events();
        }

        return mainView;
    }

    private void cameraInit(){
        // Create an instance of Camera
        camera = getCameraInstance();

        if(camera != null){
            int r = setCameraDisplayOrientation(getActivity(), Camera.CameraInfo.CAMERA_FACING_FRONT);
            camera.setDisplayOrientation(r);

            // Create our Preview view and set it as the content of our activity.
            cameraPreview = new CameraPreview(getActivity().getBaseContext(), camera);
            mainLayout = mainView.findViewById(R.id.preview_camera);
            mainLayout.addView(cameraPreview);
        }
    }

    private void recycleViewInit(){
        //recycle view
        recyclerView = mainView.findViewById(R.id.images_recycleview);
        adapter = new ImageAdapter(bitmaps, new ImageAdapter.ListItemClickListener() {
            @Override
            public void onListItemClick(BitmapWrap wrap,int position) {
                setBorderImage(wrap,position);
                setImageMainLayout(wrap);
                cameraReadyFlag = true;


            }
        });
        recyclerView.setAdapter(adapter);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getBaseContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    private void setBorderImage(BitmapWrap wrap, int position){
        if(currentBitmapWrapPosition != -1){
            currentBitmapWrap.setSelected(false);
            adapter.notifyItemChanged(currentBitmapWrapPosition);
        }
        currentBitmapWrapPosition = position;
        wrap.setSelected(true);
        adapter.notifyItemChanged(position);
    }

    private void setImageMainLayout(BitmapWrap wrap){
        mainLayout.removeView(cameraPreview);
        ImageView imageView = new ImageView(getActivity().getBaseContext());
        imageView.setImageBitmap(wrap.getBitmap());
        mainLayout.addView(imageView);
        cameraShowFlag = false;
        currentBitmapWrap = wrap;
    }


    private void events(){
        snapshotIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cameraShowFlag && cameraReadyFlag){
                    cameraReadyFlag = false;
                    camera.takePicture(null, null, MainFragment.this);
                }
                else if(cameraShowFlag==false){
                    cameraShowFlag = true;
                    mainLayout.removeAllViews();
                    mainLayout.addView(cameraPreview);
                }

            }
        });

        deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentBitmapWrap != null){
                    deleteFile(currentBitmapWrap);
                    currentBitmapWrap=null;
                    currentBitmapWrapPosition = -1;
                }
                else{
                    Toast.makeText(getActivity().getBaseContext(),R.string.toast_no_file_to_delete, Toast.LENGTH_SHORT).show();
                }

            }
        });

        discardIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.startPreview();
                cameraReadyFlag = true;
            }
        });

        flashIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(flashFlag){
                    flashLightOff();
                    flashFlag = false;
                }
                else{
                    flashFlag = true;
                    flashLightOn();
                }
            }
        });

    }

    public void flashLightOn() {

        try {
            if (getActivity().getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {

                Camera.Parameters p = camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity().getBaseContext(), R.string.toast_exception_flash_light_on,
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void flashLightOff() {
        try {
            if (getActivity().getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {
                Camera.Parameters p = camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity().getBaseContext(), R.string.toast_exception_flash_light_off,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void saveBitmap(Bitmap bitmap){

        String filename = "Trim" + (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale
                .getDefault())).format(new Date()) + ".jpg";
        BitmapWrap wrap = new BitmapWrap(filename,bitmap);

        Task task = new Task(getActivity().getBaseContext(), new Task.OnExecute() {
            @Override
            public void onExecute(BitmapWrap wrap) {
                if(wrap != null){
                    Toast.makeText(getActivity().getBaseContext(),R.string.toast_image_downloading_finish, Toast.LENGTH_SHORT).show();
                    addBitmapWrapRecycleView(wrap);
                }
            }

            @Override
            public void onPreExecute() {
                Toast.makeText(getActivity().getBaseContext(),R.string.toast_image_downloading_started, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void saveToInternalStorageError(Exception e) {
                Toast.makeText(getActivity().getBaseContext(),R.string.toast_image_downloading_error, Toast.LENGTH_SHORT).show();
            }
        });
        task.execute(wrap);
    }

    /** A safe way to get an instance of the Camera object. */
    private Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Toast.makeText(getActivity().getBaseContext(),R.string.toast_camera_not_available, Toast.LENGTH_SHORT).show();
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        BitmapFactory.Options opt;

        opt = new BitmapFactory.Options();
        opt.inTempStorage = new byte[16 * 1024];
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPictureSize();

        int height11 = size.height;
        int width11 = size.width;
        float mb = (width11 * height11) / 1024000;

        if (mb > 4f)
            opt.inSampleSize = 4;
        else if (mb > 3f)
            opt.inSampleSize = 2;

        //preview from camera
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,opt);
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);
        int orientation = setCameraDisplayOrientation(getActivity(), Camera.CameraInfo.CAMERA_FACING_FRONT);
        Bitmap bitmapRotate = rotate(bitmap, orientation);
        saveBitmap(bitmapRotate);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private void loadBitmapsRecycleView(){
        //get internal storage directory
        File f = new File(INTERNAL_STORAGE_PATH);
        File[] files = f.listFiles();
        if(files == null){
            return;
        }
        for (File file : files) {
            //load bitmap from internal storage
            Bitmap bitmap = loadImageFromStorage(INTERNAL_STORAGE_PATH,file.getName());
            if(bitmap != null){
                BitmapWrap wrap = new BitmapWrap(file.getName(),bitmap);
                //add to recycleview
                addBitmapWrapRecycleView(wrap);
            }
            else{
                Toast.makeText(getActivity().getBaseContext(),"Problem loading image "+file.getName(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Bitmap loadImageFromStorage(String path,String fileName)
    {
        Bitmap b = null;
        try {
            File f=new File(path, fileName);
            b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return b;
        }
    }

    private void addBitmapWrapRecycleView(BitmapWrap wrap){
        bitmaps.add(wrap);
        adapter.notifyItemInserted(bitmaps.size()-1);
    }

    private void deleteFile(BitmapWrap bitmap){
        File directory = new File(INTERNAL_STORAGE_PATH);
        File[] files = directory.listFiles();
        for (File file : files) {
            if(file.getName().equals(bitmap.getName())){
                file.delete();
            }
        }
        for(int i=0;i<bitmaps.size();i++){
            if(bitmaps.get(i).getName().equals(bitmap.getName())){
                bitmaps.remove(i);
                adapter.notifyItemRemoved(i);
            }
        }
    }

    public int getCorrectCameraOrientation(Camera.CameraInfo info, Camera camera) {

        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch(rotation){
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;

        }

        int result;
        if(info.facing==Camera.CameraInfo.CAMERA_FACING_FRONT){
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        }else{
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    cameraInit();

                    recycleViewInit();

                    loadBitmapsRecycleView();

                    events();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity().getBaseContext(), R.string.toast_permission_denied, Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public int  setCameraDisplayOrientation(Activity activity,
                                                   int cameraId) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;

        }
        return result;
    }


}
