package com.example.kostik.taskignitix.models;

import android.graphics.Bitmap;

/**
 * Created by kostik on 14.9.17.
 */

public class BitmapWrap {
    private String name;
    private Bitmap bitmap;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public BitmapWrap(String name, Bitmap bitmap) {
        this.name = name;
        this.bitmap = bitmap;
        this.selected = false;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
