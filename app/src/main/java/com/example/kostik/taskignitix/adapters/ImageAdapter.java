package com.example.kostik.taskignitix.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.kostik.taskignitix.R;
import com.example.kostik.taskignitix.models.BitmapWrap;

import java.util.ArrayList;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private ArrayList<BitmapWrap> images;

    final public ListItemClickListener listItemClickListener;

    public interface ListItemClickListener{
        void onListItemClick(BitmapWrap wrap,int position);
    }

    public ImageAdapter(ArrayList<BitmapWrap> images, ListItemClickListener listItemClickListener) {
        this.images = images;
        this.listItemClickListener = listItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.listitem_images,parent,false);
        return new ViewHolder(view);
    }

    @Override

    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageView;
        private LinearLayout layout;
        public ViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.image);
            imageView.setOnClickListener(this);
            layout = v.findViewById(R.id.layout_list_item);
        }

        void bind(BitmapWrap wrap){
            imageView.setImageBitmap(wrap.getBitmap());
            if(wrap.isSelected()){
                layout.setPadding(2,2,2,2);
                layout.setBackgroundColor(Color.parseColor("#2ecc71"));
            }
            else{
                layout.setPadding(0,0,0,0);
            }
        }

        @Override
        public void onClick(View view){
            int clickedPosition = getAdapterPosition();
            listItemClickListener.onListItemClick(images.get(clickedPosition), clickedPosition);

        }
    }

}
