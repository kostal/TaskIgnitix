package com.example.kostik.taskignitix.other;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.kostik.taskignitix.models.BitmapWrap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by kostik on 14.9.17.
 */

public class Task extends AsyncTask<BitmapWrap,Void,BitmapWrap> {
    private Context context;
    private OnExecute onExecute;
    public interface OnExecute{
        public void onExecute(BitmapWrap wrap);
        public void onPreExecute();
        public void saveToInternalStorageError(Exception e);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        onExecute.onPreExecute();
    }

    public Task(Context context, OnExecute onExecute) {
        this.onExecute = onExecute;
        this.context = context;
    }


    @Override
    protected BitmapWrap doInBackground(BitmapWrap... wraps) {
        return saveToInternalStorage(wraps[0]);

    }

    @Override
    protected void onPostExecute(BitmapWrap wrap) {

        onExecute.onExecute(wrap);
    }

    private BitmapWrap saveToInternalStorage(BitmapWrap wrap){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("msa_pic", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,wrap.getName());

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            wrap.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
            onExecute.saveToInternalStorageError(e);
            return null;
        } finally {

            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  wrap;
    }

}
